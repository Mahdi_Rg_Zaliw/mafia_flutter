import 'package:flutter/cupertino.dart';

class ScreenSize {
  double widthSize(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  double heightSize(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }
}
