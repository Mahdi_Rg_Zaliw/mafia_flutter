class PlayersModel {
  String name = '';
  String role = '';
  String side = '';

  PlayersModel({
    required this.name,
    required this.role,
    required this.side,
  });

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'role': role,
      'side': side,
    };
  }

  @override
  String toString() {
    return 'Players{name: $name, role: $role, side: $side}';
  }
}
