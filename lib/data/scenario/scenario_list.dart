import 'package:mafia_flutter/data/role/citizen_roles.dart';
import 'package:mafia_flutter/data/role/independent_roles.dart';
import 'package:mafia_flutter/data/role/mafia_roles.dart';
import 'package:mafia_flutter/design/images.dart';

const List scenarioItemList = [
  {
    'scenarioName': 'شب مافیا',
    'scenarioImage': devils_mafia,
    'mafiaRoles': shab_mafia_mafia_roles,
    'citizenRoles': shab_mafia_citizen_roles,
    'indepRoles': shab_mafia_indep_roles,
  },
  {
    'scenarioName': 'مافیا کلاسیک',
    'scenarioImage': scenario_classic,
    'mafiaRoles': classic_mafia_roles,
    'citizenRoles': classic_citizen_roles,
    'indepRoles': classic_indep_roles,
  },
  {
    'scenarioName': 'شب های مافیا (فیلیمو)',
    'scenarioImage': picture_mafia_one,
    'mafiaRoles': filimo_mafia_roles,
    'citizenRoles': filimo_citizen_roles,
    'indepRoles': filimo_indep_roles,
  },
  {
    'scenarioName': 'مافیا شبکه سلامت',
    'scenarioImage': scene_profesional,
    'mafiaRoles': salamat_mafia_roles,
    'citizenRoles': salamat_citizen_roles,
    'indepRoles': salamat_indep_roles,
  },
  {
    'scenarioName': 'مافیا کلاسیک پیشرفته',
    'scenarioImage': scenario,
    'mafiaRoles': professional_mafia_roles,
    'citizenRoles': professional_citizen_roles,
    'indepRoles': professional_indep_roles,
  },
];
