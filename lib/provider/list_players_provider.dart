import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mafia_flutter/models/players_model.dart';
import 'package:mafia_flutter/presentation/screens/home/home_page.dart';

class ListPlayersProvider extends ChangeNotifier {
  void onRestartGame(BuildContext context, List<PlayersModel> playersList) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => HomePageScreen(),
      ),
    );
    playersList.clear();
    notifyListeners();
  }

  String getSitutionGame(List<PlayersModel> playersList) {
    int mafia = 0;
    int citizen = 0;
    int indep = 0;
    playersList.forEach((element) {
      if (element.side == 'Mafia') {
        mafia += 1;
      } else if (element.side == 'Citizen') {
        citizen += 1;
      } else {
        indep += 1;
      }
    });
    if (mafia >= (citizen + indep)) {
      return 'Mafia Wins';
    } else if (mafia == 0 && indep == 0) {
      return 'Citizen Wins';
    } else if (indep >= (citizen + mafia)) {
      return 'Independent Wins';
    } else if (indep == 1 && mafia == 1 && citizen == 1) {
      return 'Independent Wins';
    } else {
      return 'M: $mafia \t C: $citizen \t I: $indep';
    }
    // if (citizen == mafia) {
    //   return 'Mafia Wins';
    // } else if (mafia == 0) {
    //   return 'Citizen Wins';
    // } else if ((citizen - mafia) == 1) {
    //   return 'Dangress!';
    // } else if ((citizen - mafia) == 2) {
    //   return 'Good';
    // } else if ((citizen - mafia) >= 3) {
    //   return 'Clean';
    // } else {
    //   return 'Unknow';
    // }
  }

  void removePlayerItem(item, List<PlayersModel> playersList) {
    playersList.removeAt(
      playersList.indexOf(item),
    );
    notifyListeners();
  }
}
