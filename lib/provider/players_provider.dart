import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mafia_flutter/design/theme.dart';
import 'package:mafia_flutter/models/players_model.dart';
import 'package:mafia_flutter/presentation/screens/scenario/scenario_screen.dart';
import 'package:mafia_flutter/presentation/widgets/dialog/alert_box_dialog.dart';
import 'package:mafia_flutter/presentation/widgets/dialog/exit_box_dialog.dart';
import 'package:mafia_flutter/presentation/widgets/modal/pickPlayerModa.dart';
import 'package:mafia_flutter/sql/player_provider.dart';

class PlayersProvider extends ChangeNotifier {
  TextEditingController playerController = TextEditingController();
  List<PlayersModel> playersItem = [];
  bool restoreLoading = false;
  ThemeData _themeData;
  bool themeSwitch = false;

  setThemeSwitch(bool value) {
    themeSwitch = value;
    notifyListeners();
  }

  PlayersProvider(this._themeData);

  getThemeData() => _themeData;

  setThemeData(ThemeData themeData) async {
    _themeData = themeData;
    notifyListeners();
  }

  void switchChangeThemeData(bool state) {
    if (state == false) {
      setThemeData(lightTheme);
    } else {
      setThemeData(darkTheme);
    }
    setThemeSwitch(state);
  }

  void onAddPlayersPressed(BuildContext context) {
    showPickPlayersModal(
      context,
      playerController,
      () => onPickPlayer(context),
    );
  }

  void onRemovePlayers(PlayersModel e) async {
    playersItem.removeAt(
      playersItem.indexOf(e),
    );
    notifyListeners();
    //delete from state
    var db = new PlayerProvider();
    //player provider
    await db.open();
    //open database
    await db.deletePlayers(e.name);
    //delete object from database
    await db.close();
    //close database
  }

  void goToPickRolePage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ScenarioScreen(playersList: playersItem),
      ),
    );
  }

  void getPlayersInDatabase() async {
    restoreLoading = true;
    var db = new PlayerProvider();
    await db.open();
    // open db
    var data = await db.getDataFromDatabase();
    // get data from database
    data.forEach((element) {
      playersItem.add(element);
    });
    // add database data into list
    await db.close();
    // close db
    restoreLoading = false;
    notifyListeners();
  }

  void onPickPlayer(BuildContext context) async {
    if (playerController.text.trim() != '') {
      playersItem.add(
        PlayersModel(
          name: playerController.text,
          role: 'Unknow',
          side: 'Unknow',
        ),
      );
      notifyListeners();

      var db = new PlayerProvider();
      // get Provider Db
      await db.open();
      // open db
      await db.insert(
        PlayersModel(
          name: playerController.text,
          role: 'Unknow',
          side: 'Unknow',
        ),
      );
      // insert data into database
      await db.close();
      // close db

      Navigator.pop(context);
      playerController.clear();
    } else {
      showAlertBoxDialog(context, 'Players name cannot be empty', 'Ok');
    }
  }

  Future<bool> onWillPopScopePressed(BuildContext context) {
    showExitBoxDialog(
      context,
      'Are you sure you want to exit this app ?',
      () => SystemNavigator.pop(),
      () => Navigator.pop(context),
    );
    return Future.value(false);
  }
}
