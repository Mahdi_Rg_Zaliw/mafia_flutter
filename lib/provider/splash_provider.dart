import 'package:flutter/cupertino.dart';

class SplashProvider extends ChangeNotifier {
  void goToNextPage(BuildContext context) {
    Future.delayed(const Duration(milliseconds: 4000), () {
      Navigator.pushNamed(context, 'home_page');
    });
  }
}
