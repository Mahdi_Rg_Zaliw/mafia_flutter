import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mafia_flutter/models/players_model.dart';
import 'package:mafia_flutter/presentation/screens/roles/roles_screen.dart';

class ScenarioProvider extends ChangeNotifier {
  void goToRolespageScreen(
    BuildContext context,
    List mafiaItems,
    List citizenItems,
    List indepItems,
    List<PlayersModel> playersList,
  ) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => RolesScreen(
          players: playersList,
          mafiaRoles: mafiaItems,
          citizenRoles: citizenItems,
          indepRoles: indepItems,
        ),
      ),
    );
  }
}
