import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mafia_flutter/models/players_model.dart';
import 'package:mafia_flutter/presentation/screens/roles/list_players_screen.dart';
import 'package:mafia_flutter/presentation/widgets/modal/pickCardsModal.dart';

class PickCardProvider extends ChangeNotifier {
  List<PlayersModel> playersShow = [];

  void onPickRoleCard(
    PlayersModel e,
    BuildContext context,
    List<PlayersModel> cards,
  ) {
    showPickCardsModal(
      context,
      e.name,
      e.role,
      e.side,
    );
    playersShow.add(e);
    // players add players show for ListPlayersScreen
    cards.remove(e);
    // player show his card and then remove card
    notifyListeners();
  }

  void onLetsStartGame(BuildContext context) {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => ListPlayersScreen(playersList: playersShow),
      ),
    );
  }
}
