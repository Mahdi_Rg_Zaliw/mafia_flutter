import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mafia_flutter/models/players_model.dart';
import 'package:mafia_flutter/presentation/screens/roles/pick_card_screen.dart';

class RolesProvider extends ChangeNotifier {
  List roles = [];

  void addObjectToRoles(String e, String side) {
    roles.add({
      'role': e,
      'side': side,
    });
    notifyListeners();
  }

  void removeObjectsFromList(e) {
    roles.removeWhere((item) => item['role'] == e);
    notifyListeners();
    // roles.forEach((element) {
    //   if (element['role'] == e) {
    //     roles.remove(element);
    //     notifyListeners();
    //   }
    // });
  }

  bool checkSelectedItem(String item) {
    for (var role in roles) {
      if (role['role'] == item) return true;
    }
    return false;
  }

  int checkSelectedCount(String item) {
    int count = 0;
    for (var role in roles) {
      if (role['role'] == item) {
        count += 1;
      }
    }
    return count;
  }

  void onPickCardSelected(
      BuildContext context, List<PlayersModel> players) async {
    roles.shuffle();
    roles.forEach((e) {
      players[roles.indexOf(e)].role = e['role'];
      players[roles.indexOf(e)].side = e['side'];
    });
    notifyListeners();
    await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PickCard(cards: players),
      ),
    );
    roles.clear();
  }
}
