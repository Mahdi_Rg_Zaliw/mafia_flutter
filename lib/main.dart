import 'package:flutter/material.dart';
import 'package:mafia_flutter/app.dart';
import 'package:mafia_flutter/design/theme.dart';
import 'package:mafia_flutter/provider/list_players_provider.dart';
import 'package:mafia_flutter/provider/pick_card_provider.dart';
import 'package:mafia_flutter/provider/players_provider.dart';
import 'package:mafia_flutter/provider/roles_provider.dart';
import 'package:mafia_flutter/provider/scenario_provider.dart';
import 'package:mafia_flutter/provider/splash_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => SplashProvider()),
        ChangeNotifierProvider(
            create: (context) => PlayersProvider(lightTheme)),
        ChangeNotifierProvider(create: (context) => ScenarioProvider()),
        ChangeNotifierProvider(create: (context) => RolesProvider()),
        ChangeNotifierProvider(create: (context) => PickCardProvider()),
        ChangeNotifierProvider(create: (context) => ListPlayersProvider()),
      ],
      child: MyApp(),
    ),
  );
}
