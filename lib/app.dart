import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mafia_flutter/design/theme.dart';
import 'package:mafia_flutter/provider/players_provider.dart';
import 'package:mafia_flutter/routes/routes.dart';
import 'package:provider/provider.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final playersProvider = Provider.of<PlayersProvider>(context, listen: true);
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: playersProvider.getThemeData() == darkTheme
            ? Brightness.light
            : Brightness.dark,
      ),
    );
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mafia Game',
      routes: routes,
      initialRoute: 'splash',
      theme: playersProvider.getThemeData(),
    );
  }
}
