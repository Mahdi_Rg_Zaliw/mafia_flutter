import 'package:flutter/cupertino.dart';
import 'package:mafia_flutter/presentation/screens/auth/splash_screen.dart';
import 'package:mafia_flutter/presentation/screens/home/home_page.dart';

Map<String, WidgetBuilder> routes = {
  'splash': (BuildContext context) => SplashScreen(),
  'home_page': (BuildContext context) => HomePageScreen(),
};
