import 'package:flutter/material.dart';
import 'package:mafia_flutter/models/players_model.dart';
import 'package:mafia_flutter/presentation/widgets/default_button.dart';
import 'package:mafia_flutter/presentation/widgets/pick_role_card.dart';
import 'package:mafia_flutter/provider/pick_card_provider.dart';
import 'package:provider/provider.dart';

class PickCard extends StatelessWidget {
  final List<PlayersModel> cards;
  PickCard({
    Key? key,
    required this.cards,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pickCardProvider =
        Provider.of<PickCardProvider>(context, listen: true);

    return Scaffold(
      appBar: AppBar(
        title: Text('Pick Card'),
        automaticallyImplyLeading: false,
        leading: IconButton(
          splashRadius: 25,
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.arrow_back),
        ),
      ),
      body: Container(
        child: GridView.count(
          crossAxisCount: 3,
          children: cards
              .map(
                (e) => PickRoleCard(
                  text: e.name,
                  onTap: () => {
                    pickCardProvider.onPickRoleCard(e, context, cards),
                  },
                ),
              )
              .toList(),
        ),
      ),
      bottomNavigationBar: Container(
        height: 100,
        child: DefaultButton(
          disable: cards.length != 0,
          onTap: () => pickCardProvider.onLetsStartGame(context),
          text: 'Let\'s Start',
        ),
      ),
    );
  }
}
