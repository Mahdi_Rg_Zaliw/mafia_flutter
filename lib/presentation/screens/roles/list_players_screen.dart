import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/images.dart';
import 'package:mafia_flutter/design/styles.dart';
import 'package:mafia_flutter/models/players_model.dart';
import 'package:mafia_flutter/presentation/widgets/default_button.dart';
import 'package:mafia_flutter/presentation/widgets/player_item.dart';
import 'package:mafia_flutter/provider/list_players_provider.dart';
import 'package:provider/provider.dart';

class ListPlayersScreen extends StatelessWidget {
  final List<PlayersModel> playersList;

  const ListPlayersScreen({
    Key? key,
    required this.playersList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final listPlayersProvider =
        Provider.of<ListPlayersProvider>(context, listen: true);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'List Players Screen',
        ),
        automaticallyImplyLeading: false,
        leading: IconButton(
          splashRadius: 25,
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.arrow_back),
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Container(
              height: 60,
              child: Column(
                children: [
                  GestureDetector(
                    onTap: () => {},
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Situation Game :  ',
                          style: red_22,
                        ),
                        Text(
                          listPlayersProvider.getSitutionGame(playersList),
                          style: black_22_w700,
                        ),
                        // Text(situtionGame),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView(
                children: playersList
                    .map(
                      (e) => PlayerItem(
                        onTapRemove: () => {
                          listPlayersProvider.removePlayerItem(
                            e,
                            playersList,
                          ),
                        },
                        imageCircle: picture_red_mafia,
                        nameItem: e.name,
                        roleItem: e.role,
                      ),
                    )
                    .toList(),
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Container(
        height: 100,
        child: DefaultButton(
          onTap: () => listPlayersProvider.onRestartGame(context, playersList),
          text: 'Restart Game',
        ),
      ),
    );
  }
}
