import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/images.dart';
import 'package:mafia_flutter/models/players_model.dart';
import 'package:mafia_flutter/presentation/widgets/role_card.dart';
import 'package:mafia_flutter/presentation/widgets/default_button.dart';
import 'package:mafia_flutter/provider/roles_provider.dart';
import 'package:provider/provider.dart';

class RolesScreen extends StatefulWidget {
  final List<PlayersModel> players;
  final List mafiaRoles;
  final List citizenRoles;
  final List indepRoles;
  RolesScreen({
    Key? key,
    required this.players,
    required this.mafiaRoles,
    required this.citizenRoles,
    required this.indepRoles,
  }) : super(key: key);

  @override
  _RolesScreenState createState() => _RolesScreenState();
}

class _RolesScreenState extends State<RolesScreen>
    with SingleTickerProviderStateMixin {
  TabController? tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final rolesProvider = Provider.of<RolesProvider>(context, listen: true);

    return Scaffold(
      body: Container(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 50, right: 20, left: 20, bottom: 10),
              height: 70,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(5),
                  topLeft: Radius.circular(5),
                  bottomLeft: Radius.circular(15),
                  bottomRight: Radius.circular(15),
                ),
              ),
              child: TabBar(
                labelColor: blackColor,
                unselectedLabelColor: unSelectedTabViewColor,
                indicatorColor: blackColor,
                indicatorPadding: EdgeInsets.symmetric(horizontal: 30),
                indicatorWeight: 2,
                controller: tabController,
                tabs: [
                  Tab(
                    child: Text('Mafia'),
                  ),
                  Tab(
                    child: Text('Independent'),
                  ),
                  Tab(
                    child: Text('Citizen'),
                  ),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                controller: tabController,
                children: [
                  GridView.count(
                    crossAxisCount: 3,
                    children: widget.mafiaRoles
                        .map(
                          (e) => RoleCard(
                            disableButton: rolesProvider.roles.length ==
                                widget.players.length,
                            onCardLongPressed: () =>
                                rolesProvider.removeObjectsFromList(e),
                            selectedCount: rolesProvider.checkSelectedCount(e),
                            imageBox: picture_red_mafia,
                            selected: rolesProvider.checkSelectedItem(e),
                            roleText: e,
                            onCardSelect: () =>
                                rolesProvider.addObjectToRoles(e, 'Mafia'),
                          ),
                        )
                        .toList(),
                  ),
                  GridView.count(
                    crossAxisCount: 3,
                    children: widget.indepRoles
                        .map(
                          (e) => RoleCard(
                            disableButton: rolesProvider.roles.length ==
                                widget.players.length,
                            onCardLongPressed: () =>
                                rolesProvider.removeObjectsFromList(e),
                            selectedCount: rolesProvider.checkSelectedCount(e),
                            imageBox: devils_mafia,
                            selected: rolesProvider.checkSelectedItem(e),
                            roleText: e,
                            onCardSelect: () => rolesProvider.addObjectToRoles(
                              e,
                              'Independent',
                            ),
                          ),
                        )
                        .toList(),
                  ),
                  GridView.count(
                    crossAxisCount: 3,
                    children: widget.citizenRoles
                        .map(
                          (e) => RoleCard(
                            disableButton: rolesProvider.roles.length ==
                                widget.players.length,
                            onCardLongPressed: () =>
                                rolesProvider.removeObjectsFromList(e),
                            selectedCount: rolesProvider.checkSelectedCount(e),
                            imageBox: picture_mafia_three,
                            selected: rolesProvider.checkSelectedItem(e),
                            roleText: e,
                            onCardSelect: () =>
                                rolesProvider.addObjectToRoles(e, 'Citizen'),
                          ),
                        )
                        .toList(),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: Container(
        height: 100,
        color: Colors.transparent,
        child: DefaultButton(
          disable: rolesProvider.roles.length == 0,
          onTap: () => rolesProvider.onPickCardSelected(
            context,
            widget.players,
          ),
          text: 'Pick Card',
        ),
      ),
    );
  }
}
