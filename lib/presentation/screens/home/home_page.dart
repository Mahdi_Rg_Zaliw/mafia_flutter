import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/images.dart';
import 'package:mafia_flutter/design/styles.dart';
import 'package:mafia_flutter/presentation/widgets/default_button.dart';
import 'package:mafia_flutter/presentation/widgets/default_text_button.dart';
import 'package:mafia_flutter/presentation/widgets/display_box.dart';
import 'package:mafia_flutter/presentation/widgets/player_item.dart';
import 'package:mafia_flutter/provider/players_provider.dart';
import 'package:provider/provider.dart';

class HomePageScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final playersProvider = Provider.of<PlayersProvider>(context, listen: true);

    return WillPopScope(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        floatingActionButton: FloatingActionButton(
          tooltip: 'Add Players',
          child: Padding(
            padding: EdgeInsets.all(10),
            child: Image(
              image: AssetImage(add_players),
            ),
          ),
          onPressed: () => playersProvider.onAddPlayersPressed(context),
        ),
        bottomNavigationBar: DefaultButton(
          disable: playersProvider.playersItem.length >= 6 ? false : true,
          onTap: () => playersProvider.goToPickRolePage(context),
          text: 'Pick Role',
        ),
        body: Column(
          children: [
            SizedBox(height: 40),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Theme',
                  style: default_22,
                ),
                SizedBox(width: 5),
                CupertinoSwitch(
                  trackColor: switchBlueColor,
                  activeColor: switchRedColor,
                  value: playersProvider.themeSwitch,
                  onChanged: (bool state) =>
                      playersProvider.switchChangeThemeData(state),
                ),
              ],
            ),
            DisplayBox(
              playersLength: playersProvider.playersItem.length,
            ),
            Expanded(
              child: playersProvider.playersItem.length != 0
                  ? ListView(
                      children: playersProvider.playersItem
                          .map(
                            (e) => PlayerItem(
                              onTapRemove: () => {
                                playersProvider.onRemovePlayers(e),
                              },
                              imageCircle: picture_red_mafia,
                              nameItem: 'Name: ' + e.name,
                              roleItem: 'Role: ' + e.role,
                            ),
                          )
                          .toList(),
                    )
                  : Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          'There is no player yet ... You can restore your last players',
                          style: grey_ubuntu_14,
                        ),
                        DefaultTextButton(
                          isLoading: playersProvider.restoreLoading,
                          textButton: 'Tap To Restore Players',
                          onPressed: () =>
                              playersProvider.getPlayersInDatabase(),
                        ),
                      ],
                    ),
            ),
          ],
        ),
      ),
      onWillPop: () => playersProvider.onWillPopScopePressed(context),
    );
  }
}
