import 'package:flutter/material.dart';
import 'package:mafia_flutter/data/scenario/scenario_list.dart';
import 'package:mafia_flutter/models/players_model.dart';
import 'package:mafia_flutter/presentation/widgets/scenario_item.dart';
import 'package:mafia_flutter/provider/scenario_provider.dart';
import 'package:mafia_flutter/utils/screen_size.dart';

class ScenarioScreen extends StatelessWidget {
  final List<PlayersModel> playersList;
  ScenarioScreen({Key? key, required this.playersList}) : super(key: key);

  final scenarioProvider = ScenarioProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Pick Your Scenario',
        ),
        automaticallyImplyLeading: false,
        leading: IconButton(
          splashRadius: 25,
          onPressed: () => Navigator.pop(context),
          icon: Icon(Icons.arrow_back),
        ),
      ),
      body: Container(
        // decoration: BoxDecoration(
        //   image: DecorationImage(
        //     image: AssetImage(scene_backg),
        //     fit: BoxFit.cover,
        //   ),
        // ),
        width: ScreenSize().widthSize(context),
        height: ScreenSize().heightSize(context),
        padding: EdgeInsets.only(left: 10, right: 10, top: 20),
        child: ListView(
          children: scenarioItemList
              .map(
                (e) => ScenarioItem(
                  images: e['scenarioImage'],
                  disable: false,
                  playersList: playersList,
                  scenarioText: e['scenarioName'],
                  onTap: () => scenarioProvider.goToRolespageScreen(
                    context,
                    e['mafiaRoles'],
                    e['citizenRoles'],
                    e['indepRoles'],
                    playersList,
                  ),
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
