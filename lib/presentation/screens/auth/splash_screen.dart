import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/images.dart';
import 'package:mafia_flutter/design/styles.dart';
import 'package:mafia_flutter/provider/splash_provider.dart';
import 'package:mafia_flutter/utils/screen_size.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final splashProvider = SplashProvider();

  @override
  void initState() {
    super.initState();
    splashProvider.goToNextPage(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: mainColor,
        ),
        width: ScreenSize().widthSize(context),
        height: ScreenSize().heightSize(context),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: ScreenSize().widthSize(context) / 1.8,
              height: ScreenSize().widthSize(context) / 1.8,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(mafia_splash_icon),
                ),
              ),
            ),
            SizedBox(height: 20),
            CircularProgressIndicator(
              color: blackColor,
            ),
            SizedBox(height: 20),
            Text(
              'Developed By Mahdi Zali',
              style: black_teko_15_w500,
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
