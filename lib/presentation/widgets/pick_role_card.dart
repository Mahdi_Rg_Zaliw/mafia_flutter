import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/styles.dart';

class PickRoleCard extends StatelessWidget {
  final void Function()? onTap;
  final String text;
  const PickRoleCard({
    Key? key,
    this.onTap,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(10),
            child: Container(
              padding: EdgeInsets.all(10),
              alignment: Alignment.center,
              width: 100,
              height: 100,
              decoration: BoxDecoration(
                color: cardBackgroundColor,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Text(
                text,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: mainColor,
                  fontSize: 16,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
