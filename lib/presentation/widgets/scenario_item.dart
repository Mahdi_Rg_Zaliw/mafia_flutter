import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/styles.dart';
import 'package:mafia_flutter/utils/screen_size.dart';

class ScenarioItem extends StatelessWidget {
  final List playersList;
  final String scenarioText;
  final void Function() onTap;
  final bool disable;
  final String images;
  const ScenarioItem({
    Key? key,
    required this.playersList,
    required this.scenarioText,
    required this.onTap,
    required this.disable,
    required this.images,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: ScreenSize().widthSize(context) / 1.2,
        height: ScreenSize().widthSize(context) / 2.4,
        margin: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color: blackColor,
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(images),
          ),
        ),
        child: ElevatedButton(
          onPressed: onTap,
          style: ButtonStyle(
            elevation: MaterialStateProperty.all(0),
            backgroundColor: MaterialStateProperty.all(
              buttonBackgroundColor.withOpacity(0.7),
            ),
            shadowColor: MaterialStateProperty.all(
              buttonShadowColor,
            ),
            overlayColor: MaterialStateProperty.all(
              Color.fromRGBO(180, 200, 230, .2),
            ),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
            ),
          ),
          child: Text(
            scenarioText,
            style: mainColor_22_yekan,
          ),
        ),
      ),
    );
  }
}
