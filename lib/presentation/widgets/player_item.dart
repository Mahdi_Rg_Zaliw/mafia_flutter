import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/styles.dart';

class PlayerItem extends StatelessWidget {
  final String imageCircle;
  final String nameItem;
  final String roleItem;
  final void Function()? onTapRemove;
  const PlayerItem({
    Key? key,
    required this.imageCircle,
    required this.nameItem,
    required this.roleItem,
    this.onTapRemove,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(
        left: 15,
        // top: 7.5,
        right: 15,
        bottom: 15,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
          width: 1,
          color: borderGray,
        ),
      ),
      child: Row(
        children: [
          Container(
            width: 55,
            height: 55,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(70),
              color: player_box_bg,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(imageCircle),
              ),
            ),
          ),
          SizedBox(width: 15),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  nameItem,
                  style: default_18_w300,
                ),
                SizedBox(height: 7),
                Text(
                  roleItem,
                  style: black_19_yekan,
                ),
              ],
            ),
          ),
          IconButton(
            splashRadius: 30,
            onPressed: onTapRemove,
            icon: Icon(
              Icons.delete,
              color: remove_red_color,
            ),
          )
        ],
      ),
    );
  }
}
