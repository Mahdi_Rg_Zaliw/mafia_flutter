import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/images.dart';
import 'package:mafia_flutter/design/styles.dart';
import 'package:mafia_flutter/utils/screen_size.dart';

Future showPickCardsModal(
  BuildContext context,
  String name,
  String role,
  String side,
) {
  return showModalBottomSheet(
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    builder: (BuildContext context) {
      return Container(
        height: ScreenSize().heightSize(context),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              width: 80,
              height: 10,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            Container(
              width: ScreenSize().widthSize(context),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                color: cardBackgroundColor,
              ),
              child: Column(
                children: [
                  SizedBox(height: 20),
                  Text(
                    name,
                    style: mainColor_18,
                  ),
                  SizedBox(height: 20),
                  Container(
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: AssetImage(picture_mafia_one),
                      ),
                    ),
                    width: 200,
                    height: 300,
                  ),
                  SizedBox(height: 20),
                  Text(
                    role,
                    style: mainColor_yekan_19_w700,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 20),
                  Text(
                    side,
                    style: mainColor_18,
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ],
        ),
      );
    },
    context: context,
  );
}
