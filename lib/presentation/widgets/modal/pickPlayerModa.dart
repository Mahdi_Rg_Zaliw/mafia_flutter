import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/styles.dart';
import 'package:mafia_flutter/presentation/widgets/default_input.dart';
import 'package:mafia_flutter/utils/screen_size.dart';

Future showPickPlayersModal(
  BuildContext context,
  TextEditingController playerController,
  void Function() onTap,
) {
  return showModalBottomSheet(
    isScrollControlled: true,
    backgroundColor: Colors.transparent,
    builder: (BuildContext context) {
      return Container(
        height: ScreenSize().heightSize(context),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
              width: 80,
              height: 10,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            Container(
              width: ScreenSize().widthSize(context),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30),
                  topRight: Radius.circular(30),
                ),
                color: cardBackgroundColor,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Enter Player\'s Name',
                          style: black_22,
                        ),
                        GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: Icon(
                            const IconData(0xe16a, fontFamily: 'MaterialIcons'),
                            color: mainColor,
                          ),
                        )
                      ],
                    ),
                  ),
                  DefaultInput(
                    labelText: 'Player name',
                    hintText: 'Type Player\'s Name',
                    inputController: playerController,
                  ),
                  Container(
                    margin: EdgeInsets.all(20),
                    height: 60,
                    width: ScreenSize().widthSize(context),
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          remove_red_color,
                        ),
                        shadowColor: MaterialStateProperty.all(
                          remove_red_color,
                        ),
                        overlayColor: MaterialStateProperty.all(
                          Color.fromRGBO(180, 200, 230, .2),
                        ),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                        ),
                      ),
                      onPressed: onTap,
                      child: Text(
                        'Confirm',
                        style: mainColor_default_style,
                      ),
                    ),
                    // child: DefaultButton(onTap: onTap, text: 'Confirm'),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    },
    context: context,
  );
}
