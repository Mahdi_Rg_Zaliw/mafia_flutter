import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/styles.dart';

class DisplayBox extends StatelessWidget {
  final int playersLength;
  const DisplayBox({
    Key? key,
    required this.playersLength,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      margin: EdgeInsets.only(top: 20),
      height: 80,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  Icon(Icons.gps_not_fixed_outlined),
                  SizedBox(height: 5),
                  Text(
                    'Mafia:  ${playersLength ~/ 3}',
                    style: black_ubuntu_16,
                  ),
                ],
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  Icon(Icons.people),
                  SizedBox(height: 5),
                  Text(
                    'Number Of Players:  $playersLength',
                    style: black_ubuntu_16,
                  ),
                ],
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  Icon(Icons.location_city_outlined),
                  SizedBox(height: 5),
                  Text(
                    'Citizen:  ${playersLength - playersLength ~/ 3}',
                    style: black_ubuntu_16,
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
