import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/styles.dart';

class DefaultTextButton extends StatelessWidget {
  final String textButton;
  final bool isLoading;
  final void Function()? onPressed;
  const DefaultTextButton({
    Key? key,
    required this.textButton,
    required this.isLoading,
    this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: CircularProgressIndicator(
              backgroundColor: Colors.transparent,
              color: textButtonColors,
              strokeWidth: 2,
            ),
          )
        : TextButton(
            style: ButtonStyle(
              shadowColor: MaterialStateProperty.all(
                textButtonColors,
              ),
              overlayColor: MaterialStateProperty.all(
                textButtonColors.withOpacity(0.2),
              ),
            ),
            onPressed: isLoading ? null : onPressed,
            child: Text(
              textButton,
              style: black_18_w300,
            ),
          );
  }
}
