import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/styles.dart';

class RoleCard extends StatelessWidget {
  final void Function()? onCardSelect;
  final void Function()? onCardLongPressed;
  final bool selected;
  final String roleText;
  final String imageBox;
  final int selectedCount;
  final bool disableButton;
  const RoleCard({
    Key? key,
    this.onCardSelect,
    this.onCardLongPressed,
    required this.selected,
    required this.roleText,
    required this.imageBox,
    required this.selectedCount,
    required this.disableButton,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onLongPress: onCardLongPressed,
      onTap: disableButton ? () => {} : onCardSelect,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          children: [
            Opacity(
              opacity: disableButton ? .5 : 1,
              child: Container(
                alignment: Alignment.center,
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  image: DecorationImage(
                    image: AssetImage(imageBox),
                  ),
                ),
                child: selected
                    ? Container(
                        alignment: Alignment.center,
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                          color: selected_color_circle.withOpacity(0.5),
                          borderRadius: BorderRadius.circular(80),
                        ),
                        child: Text(
                          '$selectedCount',
                          style: mainColor_18,
                        ),
                      )
                    : Container(),
              ),
            ),
            SizedBox(height: 5),
            Text(
              roleText,
              textAlign: TextAlign.center,
              style: black_13_w500_yekan,
            ),
          ],
        ),
      ),
    );
  }
}
