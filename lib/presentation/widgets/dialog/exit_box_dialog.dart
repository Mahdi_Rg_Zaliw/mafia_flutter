import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/styles.dart';
import 'package:mafia_flutter/presentation/widgets/default_text_button.dart';
import 'package:mafia_flutter/utils/screen_size.dart';

Future showExitBoxDialog(
  BuildContext context,
  String textMessage,
  void Function()? onExitPressed,
  void Function()? onNotExitPressed,
) {
  return showDialog(
    context: context,
    builder: (context) => Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        width: ScreenSize().widthSize(context),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              width: ScreenSize().widthSize(context) / 1.2,
              height: 200,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: mainColor,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(height: 10),
                  Icon(
                    Icons.logout,
                    color: player_box_bg,
                    size: 60,
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      'Exit ! \n $textMessage',
                      style: blackColor_18,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 10),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      DefaultTextButton(
                        isLoading: false,
                        textButton: 'Yes',
                        onPressed: onExitPressed,
                      ),
                      DefaultTextButton(
                        isLoading: false,
                        textButton: 'No',
                        onPressed: onNotExitPressed,
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    ),
  );
}
