import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/styles.dart';
import 'package:mafia_flutter/utils/screen_size.dart';

Future showAlertBoxDialog(
  BuildContext context,
  String textMessage,
  String closeAlertBox,
) {
  return showDialog(
    context: context,
    builder: (context) => Scaffold(
      backgroundColor: Colors.transparent,
      body: Container(
        width: ScreenSize().widthSize(context),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              width: ScreenSize().widthSize(context) / 1.4,
              height: 170,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: mainColor,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Icon(
                    Icons.warning,
                    color: remove_red_color,
                    size: 50,
                  ),
                  Text(
                    'Error ! \n $textMessage',
                    style: blackColor_18,
                    textAlign: TextAlign.center,
                  ),
                  TextButton(
                    style: ButtonStyle(
                      shadowColor: MaterialStateProperty.all(
                        blackColor,
                      ),
                      overlayColor: MaterialStateProperty.all(
                        blackColor.withOpacity(0.2),
                      ),
                    ),
                    onPressed: () => Navigator.pop(context),
                    child: Text(
                      closeAlertBox,
                      style: black_18_w300,
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    ),
  );
}
