import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/styles.dart';

class DefaultInput extends StatelessWidget {
  final TextEditingController? inputController;
  final String? hintText;
  final String? labelText;
  const DefaultInput({
    Key? key,
    this.inputController,
    this.hintText,
    this.labelText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      alignment: Alignment.center,
      margin: EdgeInsets.only(
        top: 20,
        bottom: MediaQuery.of(context).viewInsets.bottom / 1.3,
      ),
      height: 65,
      child: TextField(
        cursorColor: inputColors,
        controller: inputController,
        style: black_default_style,
        decoration: InputDecoration(
          labelText: labelText,
          hintText: hintText,
          // enabledBorder: OutlineInputBorder(
          //   borderSide: BorderSide(
          //     color: borderGray,
          //     width: 1,
          //   ),
          //   borderRadius: BorderRadius.circular(10),
          // ),
          // border: OutlineInputBorder(
          //   borderSide: BorderSide(
          //     color: blackColor,
          //     width: 1,
          //   ),
          //   borderRadius: BorderRadius.circular(15),
          // ),
          // focusedBorder: OutlineInputBorder(
          //   borderSide: BorderSide(
          //     color: blackColor,
          //     width: 1,
          //   ),
          //   borderRadius: BorderRadius.circular(10),
          // ),
          // labelStyle: grey_14,
          // hintStyle: grey_14,
        ),
      ),
    );
  }
}
