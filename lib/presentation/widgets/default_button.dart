import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/styles.dart';
import 'package:mafia_flutter/utils/screen_size.dart';

class DefaultButton extends StatelessWidget {
  final void Function()? onTap;
  final String text;
  final bool? disable;
  const DefaultButton({
    Key? key,
    required this.onTap,
    required this.text,
    this.disable,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: disable == true ? 0.3 : 1,
      child: Container(
        margin: EdgeInsets.all(20),
        width: ScreenSize().widthSize(context) / 1.4,
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
        ),
        child: ElevatedButton(
          onPressed: disable == true ? null : onTap,
          child: Text(
            text,
            style: mainColor_default_style,
          ),
        ),
      ),
    );
    ;
  }
}
