import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DatabaseProvider {
  Database? db;

  Future open({String dbName: 'Godfather.db'}) async {
    var databasePath = await getDatabasesPath();
    String path = join(databasePath, dbName);

    db = await openDatabase(
      path,
      version: 1,
      onCreate: (Database db, int version) async {
        await db
            .execute('CREATE TABLE players(name text, role text, side text)');
      },
    );
  }

  Future close() async => db?.close();
}
