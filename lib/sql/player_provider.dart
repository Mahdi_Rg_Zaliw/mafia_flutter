import 'package:mafia_flutter/models/players_model.dart';
import 'package:mafia_flutter/sql/database_provider.dart';
import 'package:sqflite/sqflite.dart';

class PlayerProvider extends DatabaseProvider {
  String tableName = 'players';

  Future<void> insert(PlayersModel players) async {
    await db!.insert(
      tableName,
      players.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<PlayersModel>> getDataFromDatabase() async {
    final List<Map<String, dynamic>> maps = await db!.query(tableName);

    // Convert the List<Map<String, dynamic> into a List<PlayersModel>.
    return List.generate(maps.length, (i) {
      return PlayersModel(
        name: maps[i]['name'],
        role: maps[i]['role'],
        side: maps[i]['side'],
      );
    });
  }

  Future<void> deletePlayers(String name) async {
    await db!.delete(
      tableName,
      // Use a `where` clause to delete a specific players.
      where: 'name = ?',
      whereArgs: [name],
    );
  }
}
