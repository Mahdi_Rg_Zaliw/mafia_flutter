import 'package:flutter/material.dart';
import 'package:mafia_flutter/design/colors.dart';
// import 'package:mafia_flutter/design/colors.dart';

const black_default_style = TextStyle(
  color: inputColors,
);

const default_22 = TextStyle(fontSize: 22);

const white_ubunto_20_w600 = TextStyle(
  color: whiteTextColor,
  fontFamily: 'ubuntu',
  fontWeight: FontWeight.w600,
  fontSize: 20,
);

const white_ubunto_20 = TextStyle(
  fontSize: 20,
  color: whiteTextColor,
  fontFamily: 'ubuntu',
);

const black_ubunto_20 = const TextStyle(
  fontSize: 20,
  color: blackColor,
  fontFamily: 'ubuntu',
);

const black_ubunto_20_w600 = TextStyle(
  color: blackColor,
  fontFamily: 'ubuntu',
  fontWeight: FontWeight.w600,
  fontSize: 20,
);

const mainColor_default_style = TextStyle(
  color: whiteTextColor,
);

const mainColor_18 = TextStyle(
  fontSize: 18,
  color: mainColor,
);

const blackColor_18 = TextStyle(
  fontSize: 18,
  color: blackColor,
);

const default_18_w300 = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w300,
);

const red_22 = TextStyle(
  fontSize: 22,
  color: remove_red_color,
  // fontWeight: FontWeight.w700,
);

const mainColor_yekan_19_w700 = TextStyle(
  fontSize: 19,
  color: mainColor,
  fontWeight: FontWeight.w700,
  fontFamily: 'yekan',
);

const grey_14 = TextStyle(
  color: inputColors,
  fontSize: 14,
);

const black_teko_15_w500 = TextStyle(
  fontFamily: 'teko',
  fontSize: 15,
  fontWeight: FontWeight.w500,
  // color: blackColor,
);

const black_22_w700 = TextStyle(
  fontSize: 22,
  fontWeight: FontWeight.w700,
);

const black_ubuntu_16 = TextStyle(
  fontFamily: 'ubuntu',
  fontSize: 16,
  // color: blackColor,
);

const grey_ubuntu_14 = TextStyle(
  fontFamily: 'ubuntu',
  fontSize: 14,
  // color: player_box_bg,
);

const black_18_w300 = TextStyle(
  fontSize: 18,
  fontWeight: FontWeight.w300,
  color: textButtonColors,
);

const black_22 = TextStyle(
  fontSize: 22,
  color: mainColor,
);

const black_13_w500_yekan = TextStyle(
  fontSize: 14,
  // color: blackColor,
  fontWeight: FontWeight.w500,
  fontFamily: 'yekan',
);

const black_19_yekan = TextStyle(
  fontSize: 19,
  // color: blackColor,
  fontWeight: FontWeight.w100,
  fontFamily: 'yekan',
);

const mainColor_22_yekan = TextStyle(
  fontSize: 25,
  color: mainColor,
  fontWeight: FontWeight.w100,
  fontFamily: 'yekan',
);
