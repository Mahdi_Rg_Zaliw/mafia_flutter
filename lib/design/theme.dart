import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mafia_flutter/design/colors.dart';
import 'package:mafia_flutter/design/styles.dart';

final darkTheme = ThemeData(
  primarySwatch: primaryMaterialColor,
  primaryColor: primaryMaterialColor,
  scaffoldBackgroundColor: blackColor,
  backgroundColor: blackColor,
  iconTheme: IconThemeData(
    color: whiteTextColor,
  ),
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: remove_red_color,
    splashColor: remove_red_color.withOpacity(0.5),
  ),
  inputDecorationTheme: InputDecorationTheme(
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: inputColors,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(10),
    ),
    border: OutlineInputBorder(
      borderSide: BorderSide(
        color: inputColors,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(15),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: inputColors,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(10),
    ),
    labelStyle: grey_14,
    hintStyle: grey_14,
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      textStyle: MaterialStateProperty.all(TextStyle(color: blackColor)),
      elevation: MaterialStateProperty.all(0),
      backgroundColor: MaterialStateProperty.all(
        remove_red_color,
      ),
      shadowColor: MaterialStateProperty.all(
        remove_red_color,
      ),
      overlayColor: MaterialStateProperty.all(
        Color.fromRGBO(180, 200, 230, .2),
      ),
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
      ),
    ),
  ),
  textTheme: const TextTheme(
    headline1: TextStyle(color: whiteTextColor),
    headline6: TextStyle(color: whiteTextColor),
    bodyText2: TextStyle(color: whiteTextColor),
    button: TextStyle(color: blackColor),
  ),
  appBarTheme: AppBarTheme(
    backgroundColor: blackColor,
    elevation: 0,
    centerTitle: true,
    titleTextStyle: white_ubunto_20_w600,
    toolbarTextStyle: white_ubunto_20,
    iconTheme: IconThemeData(
      color: inputColors,
    ),
    systemOverlayStyle: SystemUiOverlayStyle(
      systemNavigationBarColor: blackColor,
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.light,
    ),
  ),
  fontFamily: 'ubuntu',
);

final lightTheme = ThemeData(
  primarySwatch: primaryMaterialColor,
  primaryColor: primaryMaterialColor,
  scaffoldBackgroundColor: mainColor,
  backgroundColor: mainColor,
  iconTheme: IconThemeData(color: blackColor),
  textTheme: const TextTheme(
    headline1: TextStyle(color: blackColor),
    headline6: TextStyle(color: blackColor),
    bodyText2: TextStyle(color: blackColor),
  ),
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: mainColor,
    splashColor: mainColor.withOpacity(0.5),
  ),
  inputDecorationTheme: InputDecorationTheme(
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: inputColors,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(10),
    ),
    border: OutlineInputBorder(
      borderSide: BorderSide(
        color: inputColors,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(15),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: inputColors,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(10),
    ),
    labelStyle: grey_14,
    hintStyle: grey_14,
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      textStyle: MaterialStateProperty.all(
        TextStyle(color: mainColor),
      ),
      elevation: MaterialStateProperty.all(0),
      backgroundColor: MaterialStateProperty.all(
        blackColor,
      ),
      shadowColor: MaterialStateProperty.all(
        blackColor,
      ),
      overlayColor: MaterialStateProperty.all(
        Color.fromRGBO(180, 200, 230, .2),
      ),
      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
      ),
    ),
  ),
  appBarTheme: AppBarTheme(
    backgroundColor: mainColor,
    elevation: 0,
    centerTitle: true,
    titleTextStyle: black_ubunto_20_w600,
    toolbarTextStyle: black_ubunto_20,
    iconTheme: IconThemeData(
      color: blackColor,
    ),
    systemOverlayStyle: SystemUiOverlayStyle(
      systemNavigationBarColor: blackColor, // navigation bar color
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark,
    ),
  ),
  fontFamily: 'ubuntu',
);
