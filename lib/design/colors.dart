import 'package:flutter/material.dart';

const primaryMaterialColor = MaterialColor(0xFFdfe8f5, color);
const mainColor = Color(0xFFdfe8f5);
const inputColors = Color(0xFFc5c5e3);

const switchRedColor = Color(0xFFde6f81);
const switchBlueColor = Color(0xFF79b2db);

const textButtonColors = Color(0xFF888889);
const whiteTextColor = Color(0xFFdfe8f5);
const blackColor = Color(0xFF1f2124);
const inputGray = Color(0xFFBABABA);
const borderGray = Color(0xFFACACAC);
const player_box_bg = Color(0xFF555555);
const remove_red_color = Color(0xFFde1421);
const selected_color_circle = Color(0xFFB0B3B5);
const unSelectedTabViewColor = Color(0xFF9A9A9A);
const buttonBackgroundColor = Color(0xFF333333);
const buttonShadowColor = Color(0xFFBDBDBD);
const cardBackgroundColor = Color(0xFF2d2d33);

const Map<int, Color> color = {
  50: Color.fromRGBO(220, 220, 220, .1),
  100: Color.fromRGBO(220, 220, 220, .2),
  200: Color.fromRGBO(220, 220, 220, .3),
  300: Color.fromRGBO(220, 220, 220, .4),
  400: Color.fromRGBO(220, 220, 220, .5),
  500: Color.fromRGBO(220, 220, 220, .6),
  600: Color.fromRGBO(220, 220, 220, .7),
  700: Color.fromRGBO(220, 220, 220, .8),
  800: Color.fromRGBO(220, 220, 220, .9),
  900: Color.fromRGBO(220, 220, 220, 1),
};
